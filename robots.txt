# robots.txt
# This file is to prevent the crawling and indexing of certain parts
# of your site by web crawlers and spiders run by sites like Yahoo!
# and Google. By telling these "robots" where not to go on your site,
# you save bandwidth and server resources.
#
# https://www.searchenginejournal.com/robots-txt-security-risks/289719/
#
# Security wise, robots.txt usage has two rules.
#
# Do not try to implement any security through robots.txt. The robots file is
# nothing more than a kind suggestion, and while most search engine crawlers
# respect it, malicious crawlers have a good laugh and continue at their
# business. If it's linked to, it can be found.
#
# Do not expose interesting information through robots.txt. Specifically,
# if you rely on the URL to control access to certain resources (which is a
# huge alarm bell by itself), adding it to robots.txt will only make the
# problem worse: an attacker who scans robots.txt will now see the secret URL
# you were trying to hide, and concentrate efforts on that part of your site
# (you don't want it indexed, and it's named
# 'sekrit-admin-part-do-not-tell-anyone', so it's probably interesting).

# Directories
User-agent: *
Disallow: /.build/
Disallow: /.config/
Disallow: /dataset/
Disallow: /dep/
Disallow: /documentation/
Disallow: /documentation/scorecard/
Disallow: /example/
Disallow: /res/
Disallow: /res/master/
Disallow: /src/
Disallow: /test/
Disallow: /tools/

# WordPress
User-Agent: *
Disallow: /extend/themes/search.php
Disallow: /themes/search.php
Disallow: /support/rss
Disallow: /archive/
Disallow: /wp-content/plugins/
Disallow: /wp-admin/ <— Check this to confirm a WordPress installation

# Baidu
User-Agent: Baiduspider
Disallow: /

# Yandex
User-Agent: Yandex
Disallow: /
User-Agent: YandexBot
Disallow: /

# OpenAI
User-Agent: GPTBot
Disallow: /
User-Agent: ChatGPT-User
Disallow: /

# Google Bard and Vertex AI
User-Agent: Google-Extended
Disallow: /

# Common Crawl
User-Agent: CCBot
Disallow: /

# Honeybot
Disallow: /secure/logins.html

Sitemap: https://www.ndaal.eu/sitemap.xml
